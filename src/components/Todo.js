import React from "react";
import style from "./Todo.css";

const Todo = props => (
  <li className={style.Todo}>
    {props.todo.text}
    <button onClick={event => props.remove(props.id)}>x</button>
  </li>
);

export default Todo;
