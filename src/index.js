import React from "react";
import ReactDOM from "react-dom";
import App from "./containers/App";
import { hot } from "react-hot-loader";

ReactDOM.render(<App />, document.getElementById("app"));

if (module.hot) {
  module.hot.accept("./containers/App", () => {
    const NewApp = require("./containers/App").default;
    ReactDOM.render(
      <hot>
        <NewApp />
      </hot>,
      document.getElementById("app")
    );
  });
}
